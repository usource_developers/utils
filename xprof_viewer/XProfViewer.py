#!/usr/bin/env python3

import sys, json, os, matplotlib
import matplotlib.pyplot as plt

def main():
    dat = {}
    with open(sys.argv[1], "r") as fp:
        dat = json.load(fp)

    times = dat['frame_times']

    # Display the data
    mins = []
    avgs = []
    maxs = []
    samples = []
    i = 0
    for t in times:
        mins.append(1000.0 / (float(0.1 if t['min'] is 0 else t['min'])))
        maxs.append(1000.0 / (float(t['max'])))
        avgs.append(1000.0 / (float(t['avg'])))
        samples.append(i+1)

    plt.ylabel('Frame Rate (FPS)')
    plt.xlabel('Sample Number')
    plt.ylim([0, 3000])
    plt.plot(avgs, label='Average FPS')
    plt.plot(mins, 'r', label='Maximum FPS')
    plt.plot(maxs, 'g', label='Minimum FPS')
    plt.axhline(y=60, color='y', linestyle=':')
    plt.axhline(y=144, color='y', linestyle=':')
    plt.axhline(y=240, color='y', linestyle=':')
    plt.axhline(y=500, color='y', linestyle=':')
    plt.legend(loc='upper left')

    plt.show()

if __name__ == "__main__":
    main()